// CS3241Lab1.cpp : Defines the entry point for the console application.
//#include <cmath>
#include "math.h"
#include <iostream>
#include <fstream>
#include "GL\glut.h"

#define MAXPTNO 1000
#define NLINESEGMENT 32
#define NOBJECTONCURVE 8
#define OBJECTONCURVE 8
#define M_PI 3.141592654
using namespace std;

// Global variables that you can use
struct Point {
	int x,y;
};

// Storage of control points
int nPt = 0;

Point currPtList[MAXPTNO];
Point ptList[MAXPTNO];
Point C1ptList[MAXPTNO];

// Display options
bool displayControlPoints = true;
bool displayControlLines = true;
bool displayTangentVectors = false;
bool displayObjects = false;
bool C1Continuity = false;
bool eraseAllControlPoints = false;

void drawCircle(float radiusX, float radiusY,float r,float g, float b){
	glColor3f(r,g,b);
	glBegin(GL_POLYGON);
	float i;
	for(i=0;i<=360;i++){
		glVertex3f(0.0+radiusX*sin(2.0*M_PI*i/360.0),
			0.0+radiusY*cos(2.0*M_PI*i/360.0),1.0);
	}
	glEnd();
}

void drawSquare(float length, float breath){
	glPushMatrix();
	glScalef(length,breath,1);
	glBegin(GL_POLYGON);
	glVertex3f(1,1,1);
	glVertex3f(-1,1,1);
	glVertex3f(-1,-1,1);
	glVertex3f(1,-1,1);
	glEnd();
	glPopMatrix();
}

void drawCart(){
	glPushMatrix();
	glScalef(0.5,0.5,1);
		glPushMatrix();
		drawCircle(5,5,0,0,0);
		glTranslatef(8,0,0);
		drawCircle(5,5,0,0,0);
		glTranslatef(30,0,0);
		drawCircle(5,5,0,0,0);
		glPopMatrix();
		glPushMatrix();
		glTranslatef(19,-14,0);
		glColor3f(1,0,0);
		drawSquare(25,10);
		glPopMatrix();
		glPushMatrix();
		glTranslatef(30,-30,0);
		glColor3f(1,0,0);
		drawSquare(10,10);
		glPopMatrix();
	glPopMatrix();

}


void drawRightArrow()
{
	glColor3f(0,1,0);
	glBegin(GL_LINE_STRIP);
	glVertex2f(0,0);
	glVertex2f(100,0);
	glVertex2f(95,5);
	glVertex2f(100,0);
	glVertex2f(95,-5);
	glEnd();
}

double basicFunction(int n,int i,float t){
	float x1 = pow((1.0-t),(n-i));
	float x2 = pow(t,i);
	return (x1*x2);
}
double diffFunction(int n,int i,float t){
	int x = n-i;
	int y = i;

	double v1 = (pow((1.0-t),(x-1))) * pow(t,(y-1)) * (y-t*(x+y));
	return v1;
}


void createC1ContinuityyArray(){
	for(int i=0;i<nPt; i++)
	{	
		C1ptList[i].x=ptList[i].x;
		C1ptList[i].y=ptList[i].y;
		if(i>3 && (i-1)%3==0)
		{
			C1ptList[i].x=ptList[i-1].x+(ptList[i-1].x-ptList[i-2].x);
			C1ptList[i].y=ptList[i-1].y+(ptList[i-1].y-ptList[i-2].y);
		}
	}
}


void display(void)
{
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glPushMatrix();
	
	//Q4
	createC1ContinuityyArray();

	if(C1Continuity){
		memcpy(currPtList,C1ptList,nPt*sizeof(Point));
	}else{
		memcpy(currPtList,ptList,nPt*sizeof(Point));
	}

	if(displayControlPoints)
	{
		glPointSize(5);
		glBegin(GL_POINTS);
		for(int i=0;i<nPt; i++)
		{		glColor3f(0,0,0);
		if(i>3 && (i-1)%3==0)
		{
			if(C1Continuity){
				glPushMatrix();
				glColor3f(0.6,0.6,0.6);
				glVertex2d(ptList[i].x,ptList[i].y);
				glPopMatrix();
				glColor3f(1,0,0);
			}
		}
		glVertex2d(currPtList[i].x,currPtList[i].y);
		}
		glEnd();
		glPointSize(1);
	}

	//Q1
	if(displayControlLines)
	{
		glColor3f(0,1,0);
		glBegin(GL_LINE_STRIP);
		for(int i=0;i<nPt; i++)
		{
			glVertex2d(currPtList[i].x,currPtList[i].y);
		}
		glEnd();
	}

	//Q3
	glColor3f(0,0,1); //blue curves
	glBegin(GL_LINE_STRIP);
	for(int i=0;(i+3)<nPt;i+=3){
		for(float t=0.0;t<1.0;t+=(1.0/NLINESEGMENT)){
			float bezX =
				1*basicFunction(3,(0),t)*currPtList[i].x+
				3*basicFunction(3,(1),t)*currPtList[i+1].x+
				3*basicFunction(3,(2),t)*currPtList[i+2].x+
				1*basicFunction(3,(3),t)*currPtList[i+3].x;
			float bezY =
				1*basicFunction(3,(0),t)*currPtList[i].y+
				3*basicFunction(3,(1),t)*currPtList[i+1].y+
				3*basicFunction(3,(2),t)*currPtList[i+2].y+
				1*basicFunction(3,(3),t)*currPtList[i+3].y;
			glVertex2d(bezX,bezY);
		}
	}
	glEnd();
	
	if(displayObjects){
		for(int i=0;(i+3)<nPt;i+=3){
			for(float t=0.0;t<=1.0;t+=(1.0/OBJECTONCURVE)){
				glPushMatrix();
				float bezX =
					1*basicFunction(3,(0),t)*currPtList[i].x+
					3*basicFunction(3,(1),t)*currPtList[i+1].x+
					3*basicFunction(3,(2),t)*currPtList[i+2].x+
					1*basicFunction(3,(3),t)*currPtList[i+3].x;

				float bezY =
					1*basicFunction(3,(0),t)*currPtList[i].y+
					3*basicFunction(3,(1),t)*currPtList[i+1].y+
					3*basicFunction(3,(2),t)*currPtList[i+2].y+
					1*basicFunction(3,(3),t)*currPtList[i+3].y;

				float tanX =
					1*diffFunction(3,(0),t)*currPtList[i].x+
					3*diffFunction(3,(1),t)*currPtList[i+1].x+
					3*diffFunction(3,(2),t)*currPtList[i+2].x+
					1*diffFunction(3,(3),t)*currPtList[i+3].x;

				float tanY =
					1*diffFunction(3,(0),t)*currPtList[i].y+
					3*diffFunction(3,(1),t)*currPtList[i+1].y+
					3*diffFunction(3,(2),t)*currPtList[i+2].y+
					1*diffFunction(3,(3),t)*currPtList[i+3].y;

				glTranslatef(bezX,bezY,0);
				float deg = (atan(tanY/tanX)/(M_PI))*180;

				if(tanX<0){
					deg+=180;
				}

				glRotatef(deg,0,0,1);
				drawCart();
				glPopMatrix();
			}
		}
	}

	if(displayTangentVectors){
		for(int i=0;(i+3)<nPt;i+=3){
			for(float t=0.0;t<=1.0;t+=(1.0/NOBJECTONCURVE)){
				glPushMatrix();
				float bezX =
					1*basicFunction(3,(0),t)*currPtList[i].x+
					3*basicFunction(3,(1),t)*currPtList[i+1].x+
					3*basicFunction(3,(2),t)*currPtList[i+2].x+
					1*basicFunction(3,(3),t)*currPtList[i+3].x;

				float bezY =
					1*basicFunction(3,(0),t)*currPtList[i].y+
					3*basicFunction(3,(1),t)*currPtList[i+1].y+
					3*basicFunction(3,(2),t)*currPtList[i+2].y+
					1*basicFunction(3,(3),t)*currPtList[i+3].y;

				float tanX =
					1*diffFunction(3,(0),t)*currPtList[i].x+
					3*diffFunction(3,(1),t)*currPtList[i+1].x+
					3*diffFunction(3,(2),t)*currPtList[i+2].x+
					1*diffFunction(3,(3),t)*currPtList[i+3].x;

				float tanY =
					1*diffFunction(3,(0),t)*currPtList[i].y+
					3*diffFunction(3,(1),t)*currPtList[i+1].y+
					3*diffFunction(3,(2),t)*currPtList[i+2].y+
					1*diffFunction(3,(3),t)*currPtList[i+3].y;

				glTranslatef(bezX,bezY,0);
				float deg = (atan(tanY/tanX)/(M_PI))*180;

				if(t==0){
					tanY=(currPtList[i].y-currPtList[i+1].y);
					tanX=(currPtList[i].x-currPtList[i+1].x);
					deg = (atan(tanY/tanX)/(M_PI))*180;
					deg+=180;
				}

				if(t==1){
					tanY=(currPtList[i+2].y-currPtList[i+3].y);
					tanX=(currPtList[i+2].x-currPtList[i+3].x);
					deg = (atan(tanY/tanX)/(M_PI))*180;
					deg+=180;
				}

				if(tanX<0){
					deg+=180;
				}

				glRotatef(deg,0,0,1);
				drawRightArrow();
				glPopMatrix();
			}
		}
	}

	glPopMatrix();
	glutSwapBuffers ();
}

void reshape (int w, int h)
{
	glViewport (0, 0, (GLsizei) w, (GLsizei) h);
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0,w,h,0);  
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

}

void init(void)
{
	glClearColor (1.0,1.0,1.0, 1.0);
}

void readFile()
{
	std::ifstream file;
	file.open("savefile.txt");
	file >> nPt;

	if(nPt>MAXPTNO)
	{
		cout << "Error: File contains more than the maximum number of points." << endl;
		nPt = MAXPTNO;
	}

	for(int i=0;i<nPt;i++)
	{
		file >> ptList[i].x;
		file >> ptList[i].y;
	}
	file.close();// is not necessary because the destructor closes the open file by default
}

void writeFile()
{
	std::ofstream file;
	file.open("savefile.txt");
	file << nPt << endl;

	for(int i=0;i<nPt;i++)
	{
		file << ptList[i].x << " ";
		file << ptList[i].y << endl;
	}
	file.close();// is not necessary because the destructor closes the open file by default
}

void keyboard (unsigned char key, int x, int y)
{
	switch (key) {
	case 'r':
	case 'R':
		readFile();
		break;

	case 'w':
	case 'W':
		writeFile();
		break;

	case 'T':
	case 't':
		displayTangentVectors = !displayTangentVectors;
		break;

	case 'o':
	case 'O':
		displayObjects = !displayObjects;
		break;

	case 'p':
	case 'P':
		displayControlPoints = !displayControlPoints;
		break;

	case 'L':
	case 'l':
		displayControlLines = !displayControlLines;
		break;

	case 'C':
	case 'c':
		C1Continuity = !C1Continuity;
		break;

	case 'e':
	case 'E':
		// Q2
		nPt=0;
		break;

	case 'Q':
	case 'q':
		exit(0);
		break;

	default:
		break;
	}

	glutPostRedisplay();
}



void mouse(int button, int state, int x, int y)
{
	/*button: GLUT_LEFT_BUTTON, GLUT_MIDDLE_BUTTON, or GLUT_RIGHT_BUTTON */
	/*state: GLUT_UP or GLUT_DOWN */
	enum
	{
		MOUSE_LEFT_BUTTON = 0,
		MOUSE_MIDDLE_BUTTON = 1,
		MOUSE_RIGHT_BUTTON = 2,
		MOUSE_SCROLL_UP = 3,
		MOUSE_SCROLL_DOWN = 4
	};
	if((button == MOUSE_LEFT_BUTTON)&&(state == GLUT_UP))
	{
		if(nPt==MAXPTNO)
		{
			cout << "Error: Exceeded the maximum number of points." << endl;
			return;
		}
		ptList[nPt].x=x;
		ptList[nPt].y=y;
		nPt++;
	}
	glutPostRedisplay();
}

int main(int argc, char **argv)
{
	cout<<"CS3241 Lab 4"<< endl<< endl;
	cout << "Left mouse click: Add a control point"<<endl;
	cout << "Q: Quit" <<endl;
	cout << "P: Toggle displaying control points" <<endl;
	cout << "L: Toggle displaying control lines" <<endl;
	cout << "E: Erase all points (Clear)" << endl;
	cout << "C: Toggle C1 continuity" <<endl;	
	cout << "T: Toggle displaying tangent vectors" <<endl;
	cout << "O: Toggle displaying objects" <<endl;
	cout << "R: Read in control points from \"savefile.txt\"" <<endl;
	cout << "W: Write control points to \"savefile.txt\"" <<endl;
	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize (600, 600);
	glutInitWindowPosition (50, 50);
	glutCreateWindow ("CS3241 Assignment 4");
	init ();
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutMouseFunc(mouse);
	glutKeyboardFunc(keyboard);
	glutMainLoop();

	return 0;
}
