// CS3241Lab2.cpp : Defines the entry point for the console application.
#include <cmath>
#include <iostream>
#include "GL/glut.h"
#include <time.h>

using namespace std;

#define numStars 100
#define numPlanets 9

class planet
{
public:
	float distFromRef;
	float angularSpeed;
	float selfRotateSpeed;
	float selfRotateAngle;
	GLfloat color[3];
	float size;
	float angle;

	planet()
	{
		distFromRef = 0;
		selfRotateSpeed = 0;
		selfRotateAngle = 0;
		angularSpeed = 0;
		color[0] = color[1] = color[2] = 0;
		size = 0;
		angle = 0;
	}
};

GLfloat PI = 3.14;
float alpha = 0.0, k=1;
float tx = 0.0, ty=0.0;
bool toggleClock = false;
planet planetList[numPlanets];
GLfloat starColor = 1.0;
GLfloat stars[numStars][3];
float altitudeJumpSpeed = 0.01;
float altitudeJump = 0.0;
bool jumpDir = true; //up

/*Primitives
===============================================*/

void drawCircle(float radiusX, float radiusY,float r,float g, float b, float a){
	glColor4f(r,g,b,a);
	glBegin(GL_POLYGON);
	float i;
	for(i=0;i<=360;i++){
		if(i>180){
		glColor4f(r+0.3,g+0.3,b+0.3,a);
		}
		glVertex3f(0.0+radiusX*sin(2.0*PI*i/360.0),
			0.0+radiusY*cos(2.0*PI*i/360.0),1.0);
	}


	glEnd();
}
void drawRectangle(float length, float breath, float r,float g,float b, float a){
	glColor4f(r,g,b,a);
	glBegin(GL_POLYGON);
	glVertex3f(0,0,1);
	glVertex3f(0,breath,1);
	glVertex3f(length,breath,1);
	glVertex3f(length,0,1);
	glEnd();
}
void draw_torus() {
	glPushMatrix();
	glColor3f(0.5, 0.4, 0.3);
	glRotatef(110, 110, 1, 0);
	glScalef(-1, -1, 0.2);
	glutSolidTorus(0.2, 1.5, 20, 20);
	glPopMatrix();
}
void drawPixelMushroom(){
	//body
	glPushMatrix();
	drawRectangle(1.5,0.3,0.6, 0.4, 0.2,1);
	glTranslatef(-0.5,-0.6,1);
	drawRectangle(2.5,0.7,0.6, 0.4, 0.2,1);
	glTranslatef(-0.5,-1,1);
	drawRectangle(3.3,1,0.6, 0.4, 0.2,1);
	glTranslatef(1,-0.5,1);
	drawRectangle(0.5,0.5,0.6, 0.2, 0.2,1);
	glTranslatef(1,-0.2,1);
	drawRectangle(0.5,0.7,0.6, 0.2, 0.2,1);
	glPopMatrix();
	//eyes
	glPushMatrix();
	glTranslatef(0,-1,1);
	drawRectangle(0.7,1,1,1,1,1);
	glTranslatef(0.2,0.2,1);
	drawRectangle(0.3,0.5,0,0,0,1);
	glTranslatef(0.8,-0.6,1);
	drawRectangle(0.7,1,1,1,1,1);
	glTranslatef(0.2,0.2,1);
	drawRectangle(0.3,0.5,0,0,0,1);
	glPopMatrix();
}

/*Populate Data
===============================================*/
void populateStarData(){
	int i;
	for(i=0;i<numStars;i++){
		stars[i][0] = ((float(rand()) / float(RAND_MAX)) * (12 - (-12))) + (-12);
		stars[i][1] = ((float(rand()) / float(RAND_MAX)) * (12 - (-12))) + (-12);
		stars[i][2] = 0.5;
	}
}
void populatePlanetData(){

	planet sun;
	sun.angle = sun.selfRotateAngle = 0.0;
	sun.angularSpeed = 0.0;
	sun.selfRotateSpeed = 0.01;
	sun.distFromRef = 0.0;
	sun.size = 1.5;
	sun.color[0] = 1.0;
	sun.color[1] = 0.0;
	sun.color[2] = 0.0;
	planetList[0]=sun;

	planet mercury;
	mercury.angle = 45.0;

	mercury.angularSpeed = 0.08;
	mercury.selfRotateSpeed = 0.03;
	mercury.selfRotateAngle = 0.0;
	mercury.distFromRef = 2.5;
	mercury.size = 0.25;
	mercury.color[0] = 1.0;
	mercury.color[1] = 0.4;
	mercury.color[2] = 0.2;
	planetList[1]=mercury;

	planet venus;
	venus.angle = 90.0;
	venus.angularSpeed = 0.06;
	venus.selfRotateAngle = 0.0;
	venus.selfRotateSpeed = 0.01;
	venus.distFromRef = 3.5;
	venus.size = 0.6;
	venus.color[0] = 1.0;
	venus.color[1] = 1.0;
	venus.color[2] = 0.6;
	planetList[2]=venus;

	planet earth;
	earth.angle = 180.0;
	earth.selfRotateAngle = 0.0;
	earth.angularSpeed = 0.009;
	earth.selfRotateSpeed = 0.004;
	earth.distFromRef = 5.3;
	earth.size = 0.45;
	earth.color[0] = 0.0;
	earth.color[1] = 1.0;
	earth.color[2] = 0.0;
	planetList[3]=earth;

	planet mars;
	mars.angle = 270.0;
	mars.selfRotateAngle = 0.0;
	mars.angularSpeed = 0.004;
	mars.selfRotateSpeed = 0.001;
	mars.distFromRef = 7.0;
	mars.size = 0.75;
	mars.color[0] = 0.4;
	mars.color[1] = 0.0;
	mars.color[2] = 0.2;
	planetList[4]=mars;

	planet saturn;
	saturn.angle = 360.0;
	saturn.selfRotateAngle = 0.0;
	saturn.angularSpeed = 0.002;
	saturn.selfRotateSpeed = 0.005;
	saturn.distFromRef = 9.5;
	saturn.size = 0.75;
	saturn.color[0] = 0.4;
	saturn.color[1] = 0.2;
	saturn.color[2] = 0.0;
	planetList[5]=saturn;

	planet earthMoon;
	earthMoon.angle = 360.0;
	earthMoon.angularSpeed = 0.09;
	earthMoon.distFromRef = 0.85;
	earthMoon.size = 0.1;
	earthMoon.color[0] = 0.5;
	earthMoon.color[1] = 0.5;
	earthMoon.color[2] = 0.5;
	planetList[6]=earthMoon;
}

/*Animated Objects
===============================================*/
void drawPlanet(int i){
	glPushMatrix();
	glRotatef(planetList[i].selfRotateAngle,0,0,1);
	if(i==3){
		//earth's elliptical orbit
		glTranslatef((planetList[i].distFromRef+1.2)*sin(2.0*PI*(planetList[i].angle/360)),(planetList[i].distFromRef)*cos(2.0*PI*(planetList[i].angle/360)),1);
	}
	else{
		glTranslatef(planetList[i].distFromRef*sin(2.0*PI*(planetList[i].angle/360)),planetList[i].distFromRef*cos(2.0*PI*(planetList[i].angle/360)),1);
	}
	drawCircle(planetList[i].size,planetList[i].size,planetList[i].color[0],planetList[i].color[1],planetList[i].color[2],1);
	
	if(i==3){
		drawPlanet(6);
	}
	if(i==4){
		glPushMatrix();
		glScalef(0.2,0.2,1);
		glTranslatef(0,planetList[4].distFromRef,1);
		glTranslatef(0,altitudeJump,1);
		drawPixelMushroom();
		glPopMatrix();
	}
	if(i==5){
		draw_torus();
	}
	glPopMatrix();
}
void drawClockHand(unsigned char type){
	time_t seconds = time (NULL);
	struct tm * timeinfo = localtime(&seconds);
	double angle = 0.0;
	glPushMatrix();
	switch (type){
	case 'h':
		angle = 360-(float)timeinfo->tm_hour/12*360;
		glTranslatef(4*sin(-2.0*PI*(angle/360)),4*cos(-2.0*PI*(angle/360)),2);
		drawCircle(0.3,0.3,1,0,0,1);
		break;
	case 'm':
		angle = 360-(float)timeinfo->tm_min/60*360;
		glTranslatef(5*sin(-2.0*PI*(angle/360)),5*cos(-2.0*PI*(angle/360)),2);
		drawCircle(0.2,0.2,0,1,0,1);
		break;
	case 's':
		angle = 360-(float)timeinfo->tm_sec/60*360;
		glTranslatef(5*sin(-2.0*PI*(angle/360)),5*cos(-2.0*PI*(angle/360)),2);
		drawCircle(0.1,0.1,0,0,1,1);
		break;
	default:
		break;
	}
	glPopMatrix();
}
void drawStars(){
	glPushMatrix();
	glPointSize(1.5);
	glBegin(GL_POINTS);
	for (int i = 0; i<numStars; i++) {
		glColor3f(stars[i][2]*starColor, stars[i][2]*starColor, stars[i][2]*starColor);
		glVertex3f(stars[i][0], stars[i][1],1);
	}
	glEnd();
	glPopMatrix();
}

/*Object Body
===============================================*/
void drawSolarSystem(){
	int i;
	for(i=0;i<6;i++){
		drawPlanet(i);
	}
}
void drawClock(){
	glPushMatrix();
	drawClockHand('h');
	drawClockHand('m');
	drawClockHand('s');
	glPopMatrix();
}

/*Core Functions
===============================================*/

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glPushMatrix();
	//draw something here
	drawStars();
	if(toggleClock==true){
		glTranslatef(0,0,2);
		drawCircle(5,5,1,1,1,1);
		drawClock();
	}
	else{
		drawSolarSystem();
	}
	//controls transformation
	glScalef(k, k, k);	
	glTranslatef(tx, ty, 0);	
	glRotatef(alpha, 0, 0, 1);
	glPopMatrix();
	glutSwapBuffers();
}
void idle()
{
	//update animation here
	int i;
	for(i=0;i<7;i++){
		planetList[i].angle += planetList[i].angularSpeed;
		planetList[i].selfRotateAngle +=planetList[i].selfRotateSpeed;
		if(planetList[i].angle>360.0){
			planetList[i].angle =fmodf(planetList[i].angle,360.0); 
		}
		if(planetList[i].selfRotateAngle>360.0){
			planetList[i].selfRotateAngle =fmodf(planetList[i].selfRotateAngle,360.0); 
		}
	}
	starColor =((float(rand()) / float(RAND_MAX)) * (1.0 - (0.5))) + (0.5);

	if(altitudeJump > 8){
		jumpDir = false; //down
	}
	if(altitudeJump < 0){
		jumpDir = true; //up
	}
	if(jumpDir){
		altitudeJump +=altitudeJumpSpeed;
	}else{
		altitudeJump -=altitudeJumpSpeed;
	}
	glutPostRedisplay();	//after updating, draw the screen again
}
void reshape (int w, int h)
{
	glViewport (0, 0, (GLsizei) w, (GLsizei) h);

	glMatrixMode (GL_PROJECTION);
	glLoadIdentity();

	glOrtho(-10, 10, -10, 10, -10, 10);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}
void init(void)
{
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_DEPTH_TEST) ;
	glEnable(GL_BLEND);
	glClearColor (0.0, 0.0, 0.3, 1.0);
	glShadeModel (GL_SMOOTH);
	populatePlanetData();
	populateStarData();
}
void keyboard (unsigned char key, int x, int y)
{
	//keys to control scaling - k
	//keys to control rotation - alpha
	//keys to control translation - tx, ty
	switch (key) {

	case 'a':
		alpha+=10;
		glutPostRedisplay();
		break;

	case 'd':
		alpha-=10;
		glutPostRedisplay();
		break;

	case 'q':
		k+=0.1;
		glutPostRedisplay();
		break;

	case 'e':
		if(k>0.1)
			k-=0.1;
		glutPostRedisplay();
		break;

	case 'z':
		tx-=0.1;
		glutPostRedisplay();
		break;

	case 'c':
		tx+=0.1;
		glutPostRedisplay();
		break;

	case 's':
		ty-=0.1;
		glutPostRedisplay();
		break;

	case 'w':
		ty+=0.1;
		glutPostRedisplay();
		break;
	case 't':
		if(toggleClock==true)
			toggleClock=false;
		else
			toggleClock=true;
		glutPostRedisplay();
		break;
	default:
		break;
	}
}
int main(int argc, char **argv)
{
	cout<<"CS3241 Lab 2\n\n";
	cout<<"+++++CONTROL BUTTONS+++++++\n\n";
	cout<<"Scale Up/Down: Q/E\n";
	cout<<"Rotate Clockwise/Counter-clockwise: A/D\n";
	cout<<"Move Up/Down: W/S\n";
	cout<<"Move Left/Right: Z/C\n";
	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize (600, 600);
	glutInitWindowPosition (50, 50);
	glutCreateWindow (argv[0]);
	init ();
	glutDisplayFunc(display);
	glutIdleFunc(idle);
	glutReshapeFunc(reshape);	
	//glutMouseFunc(mouse);
	glutKeyboardFunc(keyboard);
	glutMainLoop();

	return 0;
}
