Your Name: How Wei Keng
Your matric number:A0087836M
########################################################
2. What you are drawing: Solar System & Clock Mode.
-animated mushroom
-animated stars
########################################################
Primitives and transformations you have used:
1.GL_LINES, GL_LINE_STRIP, GL_POLYGON,GL_TRIANGLE
2.Rotations, Translatation, Scaling
3.glutSolidTorus
########################################################
Methods you have modified:
1. display
2. main
3. idle
4. init
5. keyboard
########################################################
Any other things the TA should know?
1. Not sure if i submitted correctly, but professor said
we can just submit the .cpp file during last lecture.
########################################################
 What is the coolest thing(s) in your drawing
1. Extra feature 1: depth_test for drawing torus onto the planet saturn
2. Extra feature 2: elliptical orbit for earth
3. Fading Stars
########################################################

Thank you for your time!