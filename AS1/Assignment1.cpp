// Assignment1.cpp : Defines the entry point for the console application.
// Name: How Wei Keng
// Matric. No: A0087836M
// Theme: Snowman
#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include "GL\glut.h"
#include <time.h>

using namespace std;

GLfloat PI = 3.14;
float alpha = 0.0, k=1;
float tx = 0.0, ty=0.0;
float blink=0.0;

/*Primitives*/
void drawLine(){
	glBegin(GL_LINES);
	glVertex3f(0,5,1);
	glVertex3f(0,2,1);
	glEnd();
}
void drawBranch(){
	glBegin(GL_LINES);
	glVertex2f(0,0);
	glVertex2f(0,0.14);
	glEnd();
}
void drawCircle(double posX,double posY)
{	int i;
glBegin(GL_POLYGON);
for(i=0;i<360;i++)
	glVertex2f(sin(2.0*M_PI*i/360.0)+posX,
	cos(2.0*M_PI*i/360.0)+posY);
glEnd();
}
void drawTri(){
	glBegin(GL_TRIANGLES);
	glVertex2f(0,0.2);
	glVertex2f(-0.1,0);
	glVertex2f(0.1,0);
	glEnd();
}
/*Scenes Object*/
void drawSnow(){
	glColor3f(1,1,1);
	glPushMatrix();
	glScalef(0.07,0.07,0.07);
	drawCircle(0,0);
	glPopMatrix();
}
void drawSnowLump(){
	int i;
	glPushMatrix();
	for(int i=0; i<6; i++)
	{	glRotatef(90, 0, 0, 1);
	glTranslatef(2,2,0);
	glPushMatrix();
	for(int j=0; j<6; j++)
	{	glTranslatef(j*0.4,j*0.2,0);
	glRotatef(30, 0, 0, 1);
	drawSnow();
	}
	glPopMatrix();
	}
	glPopMatrix();
}
void drawSnowGround(){
	glPushMatrix();
	glScalef(2.5,1,1);
	glTranslatef(4,-4.5,1);
	drawSnowLump();
	glTranslatef(-1,1,1);
	drawSnowLump();
	glPopMatrix();
}
void drawStar()
{
	float starBlink = (int) blink%2000;
	glColor3f(0.86+starBlink/10,0.07+starBlink/20,0.23);
	glPushMatrix();
	glScalef(0.5,0.7,0.5);
	drawTri();
	glRotatef(180,0,0,1);
	glTranslatef(0,-0.1,0);
	drawTri();
	glPopMatrix();
}
void drawStarSnowBackground(){
	int i;
	double x,y,randNumX,randNumY;
	glPushMatrix();
	glTranslatef(0,4,1);
	for(i=0;i<66;i++){
		x = rand() % 10 + 6 + (y/10);
		y = rand() % 10 + 4 + (x/10);
		randNumX = (2.0f * ((float)rand() / (float)RAND_MAX) - 1.0f)*x;
		randNumY = (2.0f * ((float)rand() / (float)RAND_MAX) - 1.0f)*y;
		glPushMatrix();
		glTranslatef(randNumX,randNumY,1);
		glRotatef((10*randNumX/randNumY),0,0,1);
		if(randNumX<0)
			drawStar();
		else
			drawSnow();	
		glPopMatrix();
	}
	glPopMatrix();
}
void drawGround(double tx,double ty,double tz,double r,double g, double b,double ra){
	int i;
	glTranslatef(tx,ty,tz);
	glBegin(GL_POLYGON);
	glColor3f(r,g,b);
	for(i=0;i<360;i++){
		glVertex2f(ra*sin(2.0*M_PI*i/360.0),ra*cos(2.0*M_PI*i/360.0));
	}
	glEnd();
}
/*Object*/
void drawTomb(){
	glColor3f(0.41,0.41,0.41);
	glBegin(GL_POLYGON);
	glVertex3f(1.0,3,1);
	glVertex3f(1.7,2.5,1);
	glVertex3f(2,0,1);
	glVertex3f(1.5,-4.5,1);
	glVertex3f(-1.5,-4.5,1);
	glVertex3f(-2,0,1);
	glVertex3f(-1.7,2.5,1);
	glVertex3f(-1.0,3,1);
	glEnd();

	glColor3f(1,1,1);
	glLineWidth(2.0);
	drawLine();
	glPushMatrix();
	glRotatef(90,0,0,1);
	glScalef(0.6,0.6,0.6);
	glTranslatef(6,-3.5,1);
	drawLine();
	glPopMatrix();
}
void drawSnowMan(double alpha)
{
	glPushMatrix();
	glColor4f(1,1,1,alpha);
	glScalef(0.5,0.5,1);
	drawCircle(5,-13);
	glColor4f(1,1,1,1);
	glScalef(0.8,0.8,1);
	drawCircle(5.5,-14.8);
	glColor4f(0,0,0,alpha);
	glScalef(0.2,0.2,1);
	drawCircle(25,-73);
	glPopMatrix();
	glPushMatrix();
	glScalef(1,1,1);
	glTranslatef(1.8,-6.1,1);
	glColor4f(1,0.64,0,1);
	drawTri();
	glPopMatrix();
}
void draw1Wing(double x){
	glBegin(GL_POLYGON);
	for(int i=0;i<20;i++){
		if(i%2==0){
			glColor4f(1,0.89,1,0.7);
		}else{
			glColor4f(1,1,0,0.7);
		}
		glVertex2f(x*sin(2.0*3.14*i/20.0+30),cos(2.0*3.14*i/20.0));
	}
	glEnd();
}
void drawWings(double x,int r){
	int i;
	glPushMatrix();
	glRotatef(135,0,0,1);
	glTranslatef(2,2,1);
	for(i=0;i<=7;i++){
		draw1Wing(x);
		glRotatef(r,0,0,1);
	}
	glPopMatrix();
}
void drawAngel1Head(){
	glBegin(GL_POLYGON);
	for(int i=0;i<20;i++){
		glVertex2f(0.5*sin(2.0*3.14*i/20.0+20),cos(2.0*3.14*i/20.0));
	}
	glEnd();
}
void drawAngelCrown(){
	glPushMatrix();
	glTranslatef(-1.6,1.3,1);
	glColor4f(1,1,0,0.8);
	glRotatef(90,0,0,1);
	drawAngel1Head();
	glColor4f(1,1,1,0.7);
	glScalef(0.8,0.8,1);
	drawAngel1Head();
	glPopMatrix();
}
void drawSnowWomanAngel(){
	glPushMatrix();
	drawWings(2,7);
	glRotatef(7,0,0,1);
	drawWings(2.3,6);
	glPopMatrix();
	glPushMatrix();
	glScalef(-2,2,1);
	glTranslatef(-3,5,1);
	glRotatef(20,0,0,1);
	drawSnowMan(0.9);
	glPopMatrix();
	glPushMatrix();
	drawAngelCrown();
	glPopMatrix();
}
void drawFractalTreeRec(int n,int initial,int spread,int type){
	if(initial==n){
		glColor3f(0.54,0.27,0.07);
	}else{
		if(type==1){
			glColor3f(0.82,0.42,0.11);
		}
		else{
			glColor3f(0.54,0.27,0.07);
		}
	}
	drawBranch();
	if(n==0){ 
		glPushMatrix();
		glScalef(0.3,0.4,0.4);
		glTranslatef(0.1,0,1);
		if(type==1){
			drawSnow();}
		else{
			drawStar();
		}
		glPopMatrix();
		return;
	}
	glPushMatrix();
	glTranslatef(0,0.14,0);
	//right branch
	glPushMatrix();
	drawFractalTreeRec(n-1,-1,spread,type);
	glScalef(0.7,0.7,0.7);
	glPushMatrix();
	glScalef(0.3,0.3,0.3);
	if(type==1){
		drawSnow();
	}
	else{
		drawStar();
	}
	glPopMatrix();
	glPopMatrix();
	//left branch
	glPushMatrix();
	glRotatef(spread,0,0,1);
	glScalef(0.4,0.4,0.4);
	drawFractalTreeRec(n-1,-1,spread,type);
	glPushMatrix();
	glScalef(0.3,0.3,0.3);
	if(type==1){
		drawSnow();}
	else{
		drawStar();
	}
	glPopMatrix();
	glPopMatrix();
	glPopMatrix();
}
void drawSnowTree(){
	glPushMatrix();
	glScalef(-4,3,1);
	drawFractalTreeRec(8,8,25,1);
	glScalef(-1,1,1);
	drawFractalTreeRec(8,8,25,1);
	glPopMatrix();
}
void drawRedTree(){
	glPushMatrix();
	glScalef(-4,3,1);
	drawFractalTreeRec(8,10,30,0);
	glScalef(-1,1,1);
	drawFractalTreeRec(8,10,45,0);
	glPopMatrix();
}
/*Scenes*/
void drawSnowScene(){
	//218,165,32
	glBegin(GL_POLYGON);
	glColor3f(0.55, 0.7, 1.0);
	glVertex2f(10, 10);
	glColor3f(0.55, 0.7, 1.0);
	glVertex2f(10, -10);
	glColor3f(0.5, 0.63, 1.0);
	glVertex2f(0, -10);
	glColor3f(0, 0, 0.55);
	glVertex2f(0, 10);
	glEnd();

	glPushMatrix();
	drawGround(6.0,-10,1.0,0.85,0.64,0.12,5);
	glPopMatrix();

	glPushMatrix();
	drawSnowMan(1.0);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(9,-7,1);
	drawSnowTree();
	glTranslatef(-0.5,0.5,1);
	drawSnowTree();
	glPopMatrix();
	glPushMatrix();
	glTranslatef(9,-7,1);
	drawSnowGround();
	glPopMatrix();
}
void drawSpringScene(){
	//244,164,96
	glBegin(GL_POLYGON);
	glColor3f(0.52, 0.80, 0.9);
	glVertex2f(0, 10);
	glColor3f(1, 0.84, 0);
	glVertex2f(0, -10);
	glColor3f(1, 0.84, 0);
	glVertex2f(-10, -10);
	glColor3f(1, 1, 1);
	glVertex2f(-10, 10);
	glEnd();

	glPushMatrix();
	glTranslatef(-4.3,-0.3,1);
	drawGround(-5.0,-10,1.0,0.95,0.64,0.37,7.3);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-4,-4.5,1);
	glScalef(0.3,0.3,0.3);
	glRotatef(-10,0,0,1);
	drawTomb();
	glTranslatef(0.5,13,1);
	glRotatef(20,0,0,1);
	glScalef(-2,2,1);
	drawSnowMan(0.9);
	glPopMatrix();
	glPushMatrix();
	drawStarSnowBackground();
	glPopMatrix();
	glPushMatrix();
	glTranslatef(-5.5,-5,1);
	drawRedTree();
	glPopMatrix();
}
/*Essentials*/
void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glPushMatrix();
	//BackgroundStart
	//BackGround_1_Start
	drawSnowScene();
	//BackGround_2_Start
	drawSpringScene();
	//Background End

	//controls transformation
	glScalef(k, k, k);	
	glTranslatef(tx, ty, 0);	
	glRotatef(alpha, 0, 0, 1);

	//Controllable
	//AeroPlaneStart
	glPushMatrix();
	glScalef(2.9,2.9,1);
	glTranslatef(2.1,1,1);
	drawSnowWomanAngel();
	glPopMatrix();
	//AeroPlaneEnd

	glPopMatrix();
	glFlush ();
	glutSwapBuffers();
}
void idle(){
	blink++;
	if(blink==10000000){
		blink=0;
		glutPostRedisplay();
	}
}
void reshape(int w, int h)
{
	glViewport (0, 0, (GLsizei) w, (GLsizei) h);

	glMatrixMode (GL_PROJECTION);
	glLoadIdentity();

	glOrtho(-10, 10, -10, 10, -10, 10);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}
void init(void)
{
	glClearColor (1.0, 1.0, 1.0, 1.0);
	glShadeModel (GL_SMOOTH);
}
void keyboard_LAB (unsigned char key, int x, int y)
{
	//keys to control scaling - k
	//keys to control rotation - alpha
	//keys to control translation - tx, ty
	switch (key) {

	case 'a':
		alpha+=10;
		glutPostRedisplay();
		break;

	case 'd':
		alpha-=10;
		glutPostRedisplay();
		break;

	case 'q':
		k+=0.1;
		glutPostRedisplay();
		break;

	case 'e':
		if(k>0.1)
			k-=0.1;
		glutPostRedisplay();
		break;

	case 'z':
		tx-=0.1;
		glutPostRedisplay();
		break;

	case 'c':
		tx+=0.1;
		glutPostRedisplay();
		break;

	case 's':
		ty-=0.1;
		glutPostRedisplay();
		break;

	case 'w':
		ty+=0.1;
		glutPostRedisplay();
		break;

	default:
		break;
	}
}
int main(int argc, char **argv)
{
	cout<<"CS3241 Lab 1\n\n";
	cout<<"+++++CONTROL BUTTONS+++++++\n\n";
	cout<<"Scale Up/Down: Q/E\n";
	cout<<"Rotate Clockwise/Counter-clockwise: A/D\n";
	cout<<"Move Up/Down: W/S\n";
	cout<<"Move Left/Right: Z/C\n";

	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glEnable( GL_DEPTH_TEST ) ;
	glutInitWindowSize (600, 600);
	glutInitWindowPosition (50, 50);
	glutCreateWindow (argv[0]);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable( GL_BLEND );
	init();
	glutDisplayFunc(display);
	glutIdleFunc(idle);
	glutReshapeFunc(reshape);
	//glutMouseFunc(mouse);
	glutKeyboardFunc(keyboard_LAB);
	glutMainLoop();

	return 0;
}