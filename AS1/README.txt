Your Name: How Wei Keng
Your matric number:A0087836M
########################################################
2. What you are drawing: SnowMan lose a friend to Summer.
-snowman:3cirlce,1triangle
-snowwomanangle: wings: ellipse, snowman, alpha-channel-color
-tomb and cross: polygons
-star:2 triangles
-snow: circles
-fractal tree
-animated stars and snow background
########################################################
Primitives and transformations you have used:
1.GL_LINES, GL_LINE_STRIP, GL_POLYGON,GL_TRIANGLE
2.Rotations, Translatation, Scaling
########################################################
Methods you have modified:
1. display
2. main
3. added in idle for animation
########################################################
Any other things the TA should know?
1. Not sure if i submitted correctly, but professor said
we can just submit the .cpp file during last lecture.
2. did not implement the changing color of star and snow properly
########################################################
 What is the coolest thing(s) in your drawing
1. Extra feature 1: alpha coloring: enabling the depth and blend mode to use transparency for angel.
2. Extra feature 2: tried out animation for blinking and changing color star and snow. Random number for star/snow position works but colors did not change.
########################################################

Thank you for your time!