// CS3241Lab1.cpp : Defines the entry point for the console application.
//#include <cmath>
#include <Windows.h>
#include "math.h"
#include <iostream>
#include <fstream>
#include "GL\glut.h"

#define M_PI 3.141592654

// global variable

bool m_Smooth = FALSE;
bool m_Highlight = FALSE;
GLfloat angle = 0;   /* in degrees */
GLfloat angle2 = 0;   /* in degrees */
GLfloat zoom = 1.0;
float no_shininess = 0.0f;
float low_shininess = 5.0f;
float high_shininess = 100.0f;
int mouseButton = 0;
int moving, startx, starty;

#define NO_OBJECT 4;
int current_object = 0;

using namespace std;

void setupLighting()
{
	glShadeModel(GL_SMOOTH);
	glEnable(GL_NORMALIZE);

	// Lights, material properties
	GLfloat	ambientProperties[]  = {0.7f, 0.7f, 0.7f, 1.0f};
	GLfloat	diffuseProperties[]  = {0.8f, 0.8f, 0.8f, 1.0f};
	GLfloat	specularProperties[] = {1.0f, 1.0f, 1.0f, 1.0f};
	GLfloat lightPosition[] = {-100.0f,100.0f,100.0f,1.0f};

	glClearDepth( 1.0 );

	glLightfv( GL_LIGHT0, GL_POSITION, lightPosition);

	glLightfv( GL_LIGHT0, GL_AMBIENT, ambientProperties);
	glLightfv( GL_LIGHT0, GL_DIFFUSE, diffuseProperties);
	glLightfv( GL_LIGHT0, GL_SPECULAR, specularProperties);
	glLightModelf(GL_LIGHT_MODEL_TWO_SIDE, 0.0);

	// Default : lighting
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);

}

void initMaterial(float mat_ambient[]){
	float no_mat[] = {0.0f, 0.0f, 0.0f, 1.0f};
	//float mat_ambient[] = {0.3f, 0.3f, 0.3f, 1.0f};
	float mat_diffuse[] = {0.1f, 0.5f, 0.8f, 1.0f};
	float mat_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
	float mat_emission[] = {0.3f, 0.2f, 0.2f, 0.0f};

	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);

	if(m_Highlight)
	{
		glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, high_shininess);
	} else {
		glMaterialfv(GL_FRONT, GL_SPECULAR, no_mat);
		glMaterialf(GL_FRONT, GL_SHININESS, no_shininess);
	}
	glMaterialfv(GL_FRONT, GL_EMISSION, no_mat);
}
/*Primitives*/
void drawCircle(float radiusX, float radiusY, float Z){
	glBegin(GL_POLYGON);
	float i;
	for(i=0;i<=360;i++){
		glNormal3d(0,0,Z);
		glVertex3f(0.0+radiusX*sin(2.0*M_PI*i/360.0),0.0+radiusY*cos(2.0*M_PI*i/360.0),Z);
	}
	glEnd();
}
void drawSquare(){

	glBegin(GL_POLYGON); // draw one face
	glVertex3f(1,1,1);
	glVertex3f(-1,1,1);
	glVertex3f(-1,-1,1);
	glVertex3f(1,-1,1);
	glEnd();
}
void drawSphere(double xRadius, double yRadius, double zRadius)
{
	int i,j;
	int n = 20;
	for(i=0;i<n;i++)
		for(j=0;j<(2*n);j++)
			if(m_Smooth)
			{
				glBegin(GL_POLYGON);
				// SmoothShading: the normal of each vertex is actaully its own coordinates normalized for a sphere
				glNormal3d(sin(i*M_PI/n)*cos(j*M_PI/n),cos(i*M_PI/n)*cos(j*M_PI/n),sin(j*M_PI/n));
				glVertex3d(xRadius*sin(i*M_PI/n)*cos(j*M_PI/n),yRadius*cos(i*M_PI/n)*cos(j*M_PI/n),zRadius*sin(j*M_PI/n));
				glNormal3d(sin((i+1)*M_PI/n)*cos(j*M_PI/n),cos((i+1)*M_PI/n)*cos(j*M_PI/n),sin(j*M_PI/n));
				glVertex3d(xRadius*sin((i+1)*M_PI/n)*cos(j*M_PI/n),yRadius*cos((i+1)*M_PI/n)*cos(j*M_PI/n),zRadius*sin(j*M_PI/n));
				glNormal3d(sin((i+1)*M_PI/n)*cos((j+1)*M_PI/n),cos((i+1)*M_PI/n)*cos((j+1)*M_PI/n),sin((j+1)*M_PI/n));
				glVertex3d(xRadius*sin((i+1)*M_PI/n)*cos((j+1)*M_PI/n),yRadius*cos((i+1)*M_PI/n)*cos((j+1)*M_PI/n),zRadius*sin((j+1)*M_PI/n));
				glNormal3d(sin(i* M_PI/n)*cos((j+1)* M_PI/n),cos(i*M_PI/n)*cos((j+1)*M_PI/n),sin((j+1)*M_PI/n));
				glVertex3d(xRadius*sin(i*M_PI/n)*cos((j+1)*M_PI/n),yRadius*cos(i*M_PI/n)*cos((j+1)*M_PI/n),zRadius*sin((j+1)*M_PI/n));
				glEnd();
			} else	{
				// FlatShading: the normal of the whole polygon is the coordinate of the center of the polygon for a sphere
				glBegin(GL_POLYGON);
				glNormal3d(sin((i+0.5)*M_PI/n)*cos((j+0.5)*M_PI/n),cos((i+0.5)*M_PI/n)*cos((j+0.5)*M_PI/n),sin((j+0.5)*M_PI/n));
				glVertex3d(xRadius*sin(i*M_PI/n)*cos(j*M_PI/n),yRadius*cos(i*M_PI/n)*cos(j*M_PI/n),zRadius*sin(j*M_PI/n));
				glVertex3d(xRadius*sin((i+1)*M_PI/n)*cos(j*M_PI/n),yRadius*cos((i+1)*M_PI/n)*cos(j*M_PI/n),zRadius*sin(j*M_PI/n));
				glVertex3d(xRadius*sin((i+1)*M_PI/n)*cos((j+1)*M_PI/n),yRadius*cos((i+1)*M_PI/n)*cos((j+1)*M_PI/n),zRadius*sin((j+1)*M_PI/n));
				glVertex3d(xRadius*sin(i*M_PI/n)*cos((j+1)*M_PI/n),yRadius*cos(i*M_PI/n)*cos((j+1)*M_PI/n),zRadius*sin((j+1)*M_PI/n));
				glEnd();
			}
}
void drawCylinder(double radius, double height, bool isHollow)
{
	int i;
	int n = 40;
	for(i = 0; i < n; i++) {
		if(m_Smooth)
		{
			glBegin(GL_POLYGON);
			glNormal3d(sin(2.0*M_PI*i/n),+cos(2.0*M_PI*i/n),0);
			glVertex3d(radius*sin(2.0*M_PI*i/n),radius*cos(2.0*M_PI*i/n),-height);
			glNormal3d(sin(2.0*M_PI*i/n),+cos(2.0*M_PI*i/n),0);
			glVertex3d(radius*sin(2.0*M_PI*i/n),radius*cos(2.0*M_PI*i/n),height);
			glNormal3d(sin(2.0*M_PI*(i+1)/n),+cos(2.0*M_PI*(i+1)/n),0);
			glVertex3d(radius*sin(2.0*M_PI*(i+1)/n),radius*cos(2.0*M_PI*(i+1)/n),height);
			glNormal3d(sin(2.0*M_PI*(i+1)/n),+cos(2.0*M_PI*(i+1)/n),0);
			glVertex3d(radius*sin(2.0*M_PI*(i+1)/n),radius*cos(2.0*M_PI*(i+1)/n),-height);
			glEnd();
		} else 
		{
			glBegin(GL_POLYGON);
			glNormal3d(radius*sin(2.0*M_PI*(i+0.5)/n),radius*cos(2.0*M_PI*(i+0.5)/n),0);
			glVertex3d(radius*sin(2.0*M_PI*i/n),radius*cos(2.0*M_PI*i/n),-height);
			glVertex3d(radius*sin(2.0*M_PI*i/n),radius*cos(2.0*M_PI*i/n),height);
			glVertex3d(radius*sin(2.0*M_PI*(i+1)/n),radius*cos(2.0*M_PI*(i+1)/n),height);
			glVertex3d(radius*sin(2.0*M_PI*(i+1)/n),radius*cos(2.0*M_PI*(i+1)/n),-height);
			glEnd();
		}
	}
	if(!isHollow){
		drawCircle(radius, radius, height);
		drawCircle(radius, radius, -height);
	}
}
void drawCone(double radius, double height,bool isHollow)
{
	int i;
	int n = 60;
	for(i = 0; i < n; i++) {
		if(m_Smooth)
		{
			glBegin(GL_POLYGON);
			glNormal3d(0,sin(2.0*M_PI*i/n),+cos(2.0*M_PI*i/n));
			glVertex3d(0,0,-height);
			glNormal3d(sin(2.0*M_PI*i/n),+cos(2.0*M_PI*i/n),0);
			glVertex3d(radius*sin(2.0*M_PI*i/n),+radius*cos(2.0*M_PI*i/n),height);
			glNormal3d(sin(2.0*M_PI*(i+1)/n),+cos(2.0*M_PI*(i+1)/n),0);
			glVertex3d(radius*sin(2.0*M_PI*(i+1)/n),+radius*cos(2.0*M_PI*(i+1)/n),height);
			glEnd();
		} else 
		{
			glBegin(GL_POLYGON);
			glNormal3d(radius*sin(2.0*M_PI*(i+0.5)/n),radius*cos(2.0*M_PI*(i+0.5)/n),0);
			glVertex3d(radius*sin(2.0*M_PI*i/n),radius*cos(2.0*M_PI*i/n),height);
			glVertex3d(radius*sin(2.0*M_PI*(i+1)/n),radius*cos(2.0*M_PI*(i+1)/n),height);
			glVertex3d(0,0,-height);
			glEnd();
		}
	}
	if(!isHollow){
		drawCircle(radius, radius, height);
	}
}
void drawCube(float isHollow){
	glPushMatrix();
	for(int i=0;i<4;i++)
	{
		glNormal3d(0,0,1);
		drawSquare();
		glRotatef(90,0,1,0); // rotate 90 degree (x4)
	}
	glPopMatrix();

	if(!isHollow){
		glPushMatrix();
		glRotatef(90,1,0,0);
		drawSquare();
		glRotatef(180,1,0,0);
		drawSquare();
		glPopMatrix();
	}
}
void drawHepmiSphere(double xRadius, double yRadius, double zRadius)
{
	int i,j;
	int n = 20;
	for(i=0;i<n;i++)
		for(j=n;j<(2*n);j++)
			if(m_Smooth)
			{
				glBegin(GL_POLYGON);
				// SmoothShading: the normal of each vertex is actaully its own coordinates normalized for a sphere
				glNormal3d(sin(i*M_PI/n)*cos(j*M_PI/n),cos(i*M_PI/n)*cos(j*M_PI/n),sin(j*M_PI/n));
				glVertex3d(xRadius*sin(i*M_PI/n)*cos(j*M_PI/n),yRadius*cos(i*M_PI/n)*cos(j*M_PI/n),zRadius*sin(j*M_PI/n));
				glNormal3d(sin((i+1)*M_PI/n)*cos(j*M_PI/n),cos((i+1)*M_PI/n)*cos(j*M_PI/n),sin(j*M_PI/n));
				glVertex3d(xRadius*sin((i+1)*M_PI/n)*cos(j*M_PI/n),yRadius*cos((i+1)*M_PI/n)*cos(j*M_PI/n),zRadius*sin(j*M_PI/n));
				glNormal3d(sin((i+1)*M_PI/n)*cos((j+1)*M_PI/n),cos((i+1)*M_PI/n)*cos((j+1)*M_PI/n),sin((j+1)*M_PI/n));
				glVertex3d(xRadius*sin((i+1)*M_PI/n)*cos((j+1)*M_PI/n),yRadius*cos((i+1)*M_PI/n)*cos((j+1)*M_PI/n),zRadius*sin((j+1)*M_PI/n));
				glNormal3d(sin(i* M_PI/n)*cos((j+1)* M_PI/n),cos(i*M_PI/n)*cos((j+1)*M_PI/n),sin((j+1)*M_PI/n));
				glVertex3d(xRadius*sin(i*M_PI/n)*cos((j+1)*M_PI/n),yRadius*cos(i*M_PI/n)*cos((j+1)*M_PI/n),zRadius*sin((j+1)*M_PI/n));
				glEnd();
			} else	{
				// FlatShading: the normal of the whole polygon is the coordinate of the center of the polygon for a sphere
				glBegin(GL_POLYGON);
				glNormal3d(sin((i+0.5)*M_PI/n)*cos((j+0.5)*M_PI/n),cos((i+0.5)*M_PI/n)*cos((j+0.5)*M_PI/n),sin((j+0.5)*M_PI/n));
				glVertex3d(xRadius*sin(i*M_PI/n)*cos(j*M_PI/n),yRadius*cos(i*M_PI/n)*cos(j*M_PI/n),zRadius*sin(j*M_PI/n));
				glVertex3d(xRadius*sin((i+1)*M_PI/n)*cos(j*M_PI/n),yRadius*cos((i+1)*M_PI/n)*cos(j*M_PI/n),zRadius*sin(j*M_PI/n));
				glVertex3d(xRadius*sin((i+1)*M_PI/n)*cos((j+1)*M_PI/n),yRadius*cos((i+1)*M_PI/n)*cos((j+1)*M_PI/n),zRadius*sin((j+1)*M_PI/n));
				glVertex3d(xRadius*sin(i*M_PI/n)*cos((j+1)*M_PI/n),yRadius*cos(i*M_PI/n)*cos((j+1)*M_PI/n),zRadius*sin((j+1)*M_PI/n));
				glEnd();
			}
}

/*Composites*/

void drawTrain1Wheel(){
	drawCylinder(0.3,0.02,true);
	glPushMatrix();
		glRotatef(90,1,0,0);
		for(int i=0;i<6;i++){
			drawCylinder(0.02,0.3,true);
			glRotatef(30,0,1,0);
		}
	glPopMatrix();
}
void drawTrainWheels(){

	float bodyColor[]={0.0f, 0.0f, 0.0f,1.0f};

	initMaterial(bodyColor);

	glPushMatrix();
	glScalef(0.8,0.8,0.8);
	for(int i=0;i<3;i++){
		glPushMatrix();
		glTranslatef(-0.4,-0.5,0);
		glRotatef(90,0,1,0);
		drawTrain1Wheel();
	glPopMatrix();
	glPushMatrix();
		glTranslatef(0.4,-0.5,0);
		glRotatef(90,0,1,0);
		drawTrain1Wheel();
	glPopMatrix();
	glTranslatef(0,0,0.7);
	}
	glPushMatrix();
	glScalef(0.55,0.55,0.55);
	glTranslatef(-0.6,-1.09,-0.2);
	glRotatef(90,0,1,0);
	drawTrain1Wheel();
	glPopMatrix();
	glPushMatrix();
	glScalef(0.55,0.55,0.55);
	glTranslatef(0.6,-1.09,-0.2);
	glRotatef(90,0,1,0);
	drawTrain1Wheel();
	glPopMatrix();
	glPopMatrix();
}
void drawTrainHead(){

	float bodyBackColor[] = {0.2f, 0.6f, 0.2f,1.0f};
	float bodyColor[] = {0.4f, 0.0f, 0.0f,1.0f};
	float bodyLowerColor[] = {0.4f, 0.4f, 0.4f,1.0f};
	float bodyFrontColor[]={1.0f, 1.0f, 0.2f,1.0f};
	float bodyFront2Color[] = {1.0f, 0.2f, 0.0f,1.0f};
	float bodyFront3Color[]={1.0f, 0.6f, 0.2f,1.0f};

	initMaterial(bodyColor);
	drawCylinder(0.4,0.3,false);
	glPushMatrix();
	glTranslatef(0,0,1);
	initMaterial(bodyBackColor);
	drawCylinder(0.3,0.8,false);
	glPopMatrix();
	glPushMatrix();
		glTranslatef(0,0,1.8);
		glRotatef(180,1,0,0);
		initMaterial(bodyFrontColor);
		drawHepmiSphere(0.2,0.2,0.2);
		glTranslatef(0,0,-0.2);
		glRotatef(-180,1,0,0);
		initMaterial(bodyFront3Color);
		drawHepmiSphere(0.1,0.1,0.1);
		glTranslatef(0,0,0.01);
		initMaterial(bodyFront2Color);
		drawCylinder(0.05,0.05,false);
	glPopMatrix();
	glPushMatrix();
		glTranslatef(0,-0.3,0.7);
		glScalef(0.3,0.1,1);
		initMaterial(bodyLowerColor);
		drawCube(false);
	glPopMatrix();
}
void drawTrainBodyTop(){
	float bodyColor[]={0.8f, 0.6f, 0.6f,1.0f};
	initMaterial(bodyColor);
	glPushMatrix();
		glTranslatef(0,0.3,1.3);
		glScalef(0.5,0.5,0.5);
		glRotatef(90,1,0,0);
		drawCylinder(0.4,0.1,false);
		glTranslatef(0,0,-0.2);
		drawCylinder(0.2,0.5,true);
		glRotatef(180,1,0,0);
		glTranslatef(0,0,0.3);
		drawCone(0.3,0.3,true);
	glPopMatrix();
}
void drawTrainBodyTop2(){
	glPushMatrix();
	for(int i=0;i<2;i++){
		glTranslatef(0,0,0.4);
		glPushMatrix();
			glTranslatef(0,0.3,0);
			glScalef(0.3,0.3,0.3);
			glRotatef(90,1,0,0);
			drawCylinder(0.4,0.1,false);
			glTranslatef(0,0,-0.2);
			drawCylinder(0.2,0.3,true);
			glTranslatef(0,0,-0.5);
			drawCone(0.35,0.3,false);
		glPopMatrix();
	}

	glPopMatrix();
}
void drawTrainBodyBack(){

	float bodyColor[] = {0.4f, 0.2f, 0.2f,1.0f};
	float bodyTopColor[] = {0.6f, 0.6f, 0.6f,1.0f};
	float bodyInnerColor[] = {0.4f, 0.2f, 0.0f,1.0f};

	glPushMatrix();
		glTranslatef(0,0,-0.65);
		glPushMatrix();
		glTranslatef(0,-0.5,-0.1);
		glScalef(0.4,0.03,0.5);
			initMaterial(bodyColor);
			drawCube(false);
		glPopMatrix();
		glPushMatrix();
		glRotatef(90,0,0,1);
		glScalef(0.5,0.4,0.4);
			drawCube(true);
		glPopMatrix();

		glPushMatrix();
		glRotatef(90,1,0,0);
		glTranslatef(0,0,-1);
			initMaterial(bodyTopColor);
			drawCone(0.8,0.5,false);
		glPopMatrix();

		glPushMatrix();
			glTranslatef(0,-0.3,0.1);
			glScalef(0.4,0.2,0.3);
			initMaterial(bodyInnerColor);
			drawCube(false);
		glPopMatrix();
	glPopMatrix();
}
void drawTrain(){
	drawTrainHead();
	drawTrainWheels();
	drawTrainBodyTop();
	drawTrainBodyTop2();
	drawTrainBodyBack();
}
void drawTentacleRecurisve(int n){
	drawCone(0.1,0.6,false);
	if(n==0) return;
	glPushMatrix();
		glTranslatef(0,0,-0.25);
		glScalef(0.7,0.7,0.7);
		glRotatef(10,1,0,0);
		drawTentacleRecurisve(n-1);
	glPopMatrix();

}
void drawTentaclesXY(float radius, float dir){
	float currdir = dir;
	glPushMatrix();
	for(int i=0;i<360;i++){
		if(i%20==0){
		glPushMatrix();
		glTranslatef(radius*sin(2.0*M_PI*i/360.0),radius*cos(2.0*M_PI*i/360.0),currdir);
		drawTentacleRecurisve(8);
		glPopMatrix();
		}
	}
	glPopMatrix();
}
void drawTentaclesXYZ(float radius, float dir){
	float currdir = dir;
	glPushMatrix();
	for(int i=0;i<360;i++){
		if(i%60==0){
		glPushMatrix();
		glTranslatef(radius*sin(2.0*M_PI*i/360.0),radius*cos(2.0*M_PI*i/360.0),currdir);
		glRotatef(15,1,0,0);
		drawTentacleRecurisve(8);
		glPopMatrix();
		}
	}
	glPopMatrix();
}
void drawOctopus(){
	float bodyColor[] = {0.8f,0.6f,0.2f,1.0f};
	float wingColor[] = {0.8f, 0.8f, 0.8f, 0.8f};
	float noseColor[] ={0.6f, 0.4f, 0.4f,1.0f};
	float eyeWhiteColor[]={1.0,1.0,1.0,1.0f};
	float eyeBlackColor[]={0.0,0.0,0.0,1.0f};
	float tentacleColor[] = {0.6f, 0.8f, 0.6f,1.0f};
	float hatTopColor[]={0.8, 0.6, 0.2,1.0f};


	glPushMatrix();
	//body
	initMaterial(bodyColor);
	drawSphere(1.0,1.0,1.0);
	glPushMatrix();
	glRotatef(-90,1,0,0);
	glTranslatef(0,0,-2);
	initMaterial(tentacleColor);
	drawTentaclesXYZ(0.65,1.0);
	glPopMatrix();
	
	glPushMatrix();
	initMaterial(noseColor);
	drawTentacleRecurisve(8);
		glPushMatrix();
		glTranslatef(0,0,0.05);
		drawCylinder(0.3,1,false);
		glTranslatef(0,0,0.05);
		drawCylinder(0.2,1,false);
		glTranslatef(0,0,0.05);
		drawCylinder(0.1,1,false);
		glTranslatef(0,0,0.05);
		drawCylinder(0.08,1,false);
		glPopMatrix();

		glPushMatrix();
			glPushMatrix();
			glTranslatef(-0.25,0.5,0.8);
			initMaterial(eyeWhiteColor);
			drawSphere(0.3,0.3,0.3);
			glTranslatef(0,0,0.3);
			initMaterial(eyeBlackColor);
			drawSphere(0.1,0.1,0.1);
			glPopMatrix();
			glPushMatrix();
			glTranslatef(0.25,0.5,0.8);
			initMaterial(eyeWhiteColor);
			drawSphere(0.3,0.3,0.3);
			glTranslatef(0,0,0.3);
			initMaterial(eyeBlackColor);
			drawSphere(0.1,0.1,0.1);
			glPopMatrix();
		glPopMatrix();
	glPopMatrix();
	//wings
	initMaterial(wingColor);
	glPushMatrix();
		glTranslatef(-1,0,0);
		glRotatef(100,0,0,1);
		drawSphere(0.09,1.5,0.3);
		glTranslatef(0,-0.1,-0.1);
		glRotatef(-15,0,0,1);
		drawSphere(0.09,1.3,0.3);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(1,0,0);
		glRotatef(-100,0,0,1);
		drawSphere(0.09,1.5,0.3);
		glTranslatef(0,0.1,0.1);
		glRotatef(15,0,0,1);
		drawSphere(0.09,1.3,0.3);
	glPopMatrix();

	glPopMatrix();
}

void drawSpiral(float r,int startDeg, int endDeg){
	glPushMatrix();
	glTranslatef(0,0,-60);
	glScalef(0.5,0.5,0.5);
int i;
	for(i=startDeg;i<=endDeg;i++){
		if(i%45==0){
		glTranslatef(r*cos(2.0*M_PI*i/360.0),r*sin(2.0*M_PI*i/360.0),0.05);
		drawOctopus();
		glScalef(1.2,1.2,1.2);
		r+=0.5;
		}
	}
	glPopMatrix();
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glPushMatrix();
	glTranslatef(0, 0, -6);

	glRotatef(angle2, 1.0, 0.0, 0.0);
	glRotatef(angle, 0.0, 1.0, 0.0);

	glScalef(zoom,zoom,zoom);
	float primitiveColor[] = {0.3f, 0.3f, 0.3f, 1.0f};
	switch (current_object) {
	case 0:{
		initMaterial(primitiveColor);
		drawSphere(1.0,1.0,1.0);
		break;}
	case 1:
		// draw your second primitive object here
		initMaterial(primitiveColor);
		drawCylinder(0.5,1,false);
		break;
	case 2:
		// draw your first composite object here
		drawSpiral(0.3,0,540);
		break;
	case 3:
		drawTrain();
		// draw your second composite object here

		break;
	default:
		break;
	};
	glPopMatrix();
	glutSwapBuffers ();
}

void keyboard (unsigned char key, int x, int y)
{
	switch (key) {
	case 'p':
	case 'P':
		glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
		break;			
	case 'w':
	case 'W':
		glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
		break;			
	case 'v':
	case 'V':
		glPolygonMode(GL_FRONT_AND_BACK,GL_POINT);
		break;			
	case 's':
	case 'S':
		m_Smooth = !m_Smooth;
		break;
	case 'h':
	case 'H':
		m_Highlight = !m_Highlight;
		break;

	case '1':
	case '2':
	case '3':
	case '4':
		current_object = key - '1';
		break;

	case 'Q':
	case 'q':
		exit(0);
		break;

	default:
		break;
	}

	glutPostRedisplay();
}

void mouse(int button, int state, int x, int y)
{
	if (state == GLUT_DOWN) {
		mouseButton = button;
		moving = 1;
		startx = x;
		starty = y;
	}
	if (state == GLUT_UP) {
		mouseButton = button;
		moving = 0;
	}
}

void motion(int x, int y)
{
	if (moving) {
		if(mouseButton==GLUT_LEFT_BUTTON)
		{
			angle = angle + (x - startx);
			angle2 = angle2 + (y - starty);
		}
		else zoom += ((y-starty)*0.001);
		startx = x;
		starty = y;
		glutPostRedisplay();
	}

}

int main(int argc, char **argv)
{
	cout<<"CS3241 Lab 3"<< endl<< endl;

	cout << "1-4: Draw different objects"<<endl;
	cout << "S: Toggle Smooth Shading"<<endl;
	cout << "H: Toggle Highlight"<<endl;
	cout << "W: Draw Wireframe"<<endl;
	cout << "P: Draw Polygon"<<endl;
	cout << "V: Draw Vertices"<<endl;
	cout << "Q: Quit" <<endl<< endl;

	cout << "Left mouse click and drag: rotate the object"<<endl;
	cout << "Right mouse click and drag: zooming"<<endl;

	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize (600, 600);
	glutInitWindowPosition (50, 50);
	glutCreateWindow ("CS3241 Assignment 3");
	glClearColor (1.0,1.0,1.0, 1.0);
	glutDisplayFunc(display);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	glutKeyboardFunc(keyboard);
	setupLighting();
	glDisable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST); 
	glDepthMask(GL_TRUE);
	glMatrixMode(GL_PROJECTION);
	gluPerspective( /* field of view in degree */ 40.0,
		/* aspect ratio */ 1.0,
		/* Z near */ 1.0, /* Z far */ 80.0);
	glMatrixMode(GL_MODELVIEW);
	glutMainLoop();

	return 0;
}
