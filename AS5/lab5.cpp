#include "drawHouses.h"

void drawA0087836Sphere(double xRadius, double yRadius, double zRadius)
{
	float M_PI = 3.14159;
	int i,j;
	int n = 16;
	for(i=0;i<n;i++)
		for(j=0;j<(2*n);j++)
		{
			glBegin(GL_POLYGON);
			glTexCoord2d(0,0);
			glVertex3d(xRadius*sin(i*M_PI/n)*cos(j*M_PI/n),yRadius*cos(i*M_PI/n)*cos(j*M_PI/n),zRadius*sin(j*M_PI/n));
			glTexCoord2d(1,0);
			glVertex3d(xRadius*sin((i+1)*M_PI/n)*cos(j*M_PI/n),yRadius*cos((i+1)*M_PI/n)*cos(j*M_PI/n),zRadius*sin(j*M_PI/n));
			glTexCoord2d(1,1);
			glVertex3d(xRadius*sin((i+1)*M_PI/n)*cos((j+1)*M_PI/n),yRadius*cos((i+1)*M_PI/n)*cos((j+1)*M_PI/n),zRadius*sin((j+1)*M_PI/n));
			glTexCoord2d(0,1);
			glVertex3d(xRadius*sin(i*M_PI/n)*cos((j+1)*M_PI/n),yRadius*cos(i*M_PI/n)*cos((j+1)*M_PI/n),zRadius*sin((j+1)*M_PI/n));
			glEnd();
		}
}
void drawA0087836Cube(){
	glPushMatrix();
	for(int i=0;i<4;i++)
	{
		glBegin(GL_POLYGON);
		glTexCoord2d(0,0);glVertex3f(1,1,1);
		glTexCoord2d(1,0);glVertex3f(-1,1,1);
		glTexCoord2d(1,1);glVertex3f(-1,-1,1);
		glTexCoord2d(0,1);glVertex3f(1,-1,1);
		glEnd();	
		glRotatef(90,0,1,0); // rotate 90 degree (x4)
	}
	glPushMatrix();
	glRotatef(90,1,0,0);
	glBegin(GL_POLYGON);
	glTexCoord2d(0,0);glVertex3f(1,1,1);
	glTexCoord2d(1,0);glVertex3f(-1,1,1);
	glTexCoord2d(1,1);glVertex3f(-1,-1,1);
	glTexCoord2d(0,1);glVertex3f(1,-1,1);
	glEnd();
	glRotatef(180,1,0,0);
	glBegin(GL_POLYGON);
	glTexCoord2d(0,0);glVertex3f(1,1,1);
	glTexCoord2d(1,0);glVertex3f(-1,1,1);
	glTexCoord2d(1,1);glVertex3f(-1,-1,1);
	glTexCoord2d(0,1);glVertex3f(1,-1,1);
	glEnd();
	glPopMatrix();
	glPopMatrix();
}
void drawA0087836Rectangle(){
	glPushMatrix();
	//for(int i=0;i<4;i++)
	//{
	glBegin(GL_POLYGON);
	glTexCoord2d(0,0);glVertex3f(1,1,1);
	glTexCoord2d(1,0);glVertex3f(-1,1,1);
	glTexCoord2d(1,1);glVertex3f(-1,-1,1);
	glTexCoord2d(0,1);glVertex3f(1,-1,1);
	glEnd();	
	//glRotatef(90,0,1,0); // rotate 90 degree (x4)
	//}
	glPopMatrix();
}
void drawA0087836Cone(double radius, double height)
{
	float M_PI = 3.14159;
	int i;
	int n = 20;
	for(i = 0; i < n; i++) {
		glBegin(GL_POLYGON);
		glTexCoord2d(0,0);
		glVertex3d(0,0,-height);
		glTexCoord2d(1,0);
		glVertex3d(radius*sin(2.0*M_PI*i/n),+radius*cos(2.0*M_PI*i/n),height);
		glTexCoord2d(1,1);
		glVertex3d(radius*sin(2.0*M_PI*(i+1)/n),+radius*cos(2.0*M_PI*(i+1)/n),height);
		glEnd();
	}
}
void drawA0087836Cylinder(double radius, double height)
{
	float M_PI = 3.14159;
	int i;
	int n = 40;
	for(i = 0; i < n; i++) {
		glBegin(GL_POLYGON);
		glTexCoord2d(0,0);
		glVertex3d(radius*sin(2.0*M_PI*i/n),radius*cos(2.0*M_PI*i/n),-height);
		glTexCoord2d(1,0);
		glVertex3d(radius*sin(2.0*M_PI*i/n),radius*cos(2.0*M_PI*i/n),height);
		glTexCoord2d(1,1);
		glVertex3d(radius*sin(2.0*M_PI*(i+1)/n),radius*cos(2.0*M_PI*(i+1)/n),height);
		glTexCoord2d(1,1);
		glVertex3d(radius*sin(2.0*M_PI*(i+1)/n),radius*cos(2.0*M_PI*(i+1)/n),-height);
		glEnd();
	}
}
void drawA0087836Circle(float radiusX, float radiusY){
	float M_PI = 3.14159;
	glBegin(GL_POLYGON);
	float i;
	for(i=0;i<=360;i++){
		glVertex3f(0.0+radiusX*sin(2.0*M_PI*i/360.0),0.0+radiusY*cos(2.0*M_PI*i/360.0),1);
	}
	glEnd();
}
void A0087836House(GLuint texSet[])
{
	const float DEG2RAD = 3.14159/180;
	float degInRad, radius=2;
	//plot dimension
	//x: -4:4
	//y: 0:12
	//z: -4:4
	//bounding volume
	glPushMatrix();

	//==##DRAW FOG on TOP==//
	//==DRAW FOG on TOP##==//

	//==##DRAW CLOUDS==//
	//==DRAW CLOUDS##==//

	//==##DRAW LIGHTNING==//
	//==DRAW LIGHTNING##==//

	//==##DRAW RAIN==//
	//==DRAW RAIN##==//

	//==##DRAW BIG INSECT==//
	//##DRAW HEAD
	glPushMatrix();
	glScalef(0.4,0.4,0.4);
	glTranslatef(0,15,0);
	glPushMatrix();

	glRotatef(180,0,1,0);
	//INSECT FACE
	glTranslatef(2.0,9,0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture( GL_TEXTURE_2D, texSet[8] );
	drawA0087836Sphere(1.0,1.0,1.0);
	glDisable(GL_TEXTURE_2D);

	glTranslatef(1.0,0,0);

	//##INSECT nose
	glPushMatrix();
	glRotatef(90,0,1,0);
	glColor3f(0.6,0.6,0.6);
	glutSolidCone(0.2, 0.5, 16, 16);
	glPopMatrix();
	//INSECT nose##

	//##EYES
	glPushMatrix();
	glTranslatef(-0.2,0.2,0.5);
	glEnable(GL_TEXTURE_2D);
	glBindTexture( GL_TEXTURE_2D, texSet[30] );
	drawA0087836Sphere(0.4,0.4,0.4);
	glTranslatef(0,0,-1.0);
	drawA0087836Sphere(0.4,0.4,0.4);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	//##EYES

	glPopMatrix();
	//DRAW HEAD##
	//##INSECT FACE

	//##INSECT BODY
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture( GL_TEXTURE_2D, texSet[7] );
	glTranslatef(0,8.5,0);
	glScalef(1.2,1,1);
	drawA0087836Sphere(1.2,1.2,1.2);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	//INSECT BODY##

	//##INSECT WINGS
	glPushMatrix();
	glColor3f(1,1,1);
	glTranslatef(0,9.0,-2.4);
	//rightwing
	glPushMatrix();
	glPushMatrix();
	glRotatef(15,1,0,0);
	glScalef(0.65,0.1,1.3);
	glutWireSphere(1.2, 24, 24);
	glPopMatrix();

	glPushMatrix();
	glRotatef(-15,1,0,0);
	glScalef(0.65,0.1,1.3);
	glTranslatef(0,-5.5,0);
	glutWireSphere(1.2, 24, 24);
	glPopMatrix();
	glPopMatrix();
	//lefttwing
	glTranslatef(0,0,4.8);
	//rightwing
	glPushMatrix();
	glPushMatrix();
	glRotatef(-15,1,0,0);
	glScalef(0.65,0.1,1.3);
	glutWireSphere(1.2, 24, 24);
	glPopMatrix();

	glPushMatrix();
	glRotatef(15,1,0,0);
	glScalef(0.65,0.1,1.3);
	glTranslatef(0,-5.5,0);
	glutWireSphere(1.2, 24, 24);
	glPopMatrix();
	glPopMatrix();

	glPopMatrix();
	glPopMatrix();
	//INSECT WINGS##
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0,5,0);
	glScalef(0.5,0.5,0.5);
	//stick1
	glPushMatrix();
	glColor3f(1,0.6,0.6);
	glTranslatef(0.3,7,0);
	glRotatef(70,1,0,0);
	drawA0087836Cylinder(0.04,2);
	//gluCylinder(qObj,0.1,0.1,5,16,16);
	glPopMatrix();

	//stick2
	glPushMatrix();
	glRotatef(180,0,1,0);
	glTranslatef(-0.3,7,0);
	glRotatef(70,1,0,0);
	drawA0087836Cylinder(0.04,2);
	//gluCylinder(qObj,0.1,0.1,5,16,16);
	glPopMatrix();
	glPopMatrix();



	//==DRAW BIG INSEECT##==//

	//==##DRAW HOUSE==//
	glPushMatrix();

	//==##DRAW HOUSEROOF==//
	glPushMatrix();
	glRotatef(90,1,0,0);
	glTranslatef(0,0,-7.5);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texSet[30]);
	drawA0087836Cone(1,1.0);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	//==DRAW HOUSEROOF##==//
	//==##DRAW HOUSEROOFBODY==//
	glPushMatrix();
	glTranslatef(0,6,0);
	glScalef(0.5,0.8,0.5);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texSet[1]);
	drawA0087836Cube();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	//==DRAW HOUSEROOFBODY##==//

	//==##DRAW HOUSEROOFFANS==//
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texSet[32]);
	glTranslatef(0,6.9,0);
	glRotatef(90,0,1,0);
	glPushMatrix();
	glScalef(1.6,0.5,1);
	glTranslatef(-1.2,0,0);
	glRotatef(5,1,0,0);
	drawA0087836Rectangle();
	glPopMatrix();
	glPushMatrix();
	glScalef(1.6,0.5,1);
	glTranslatef(1.2,0,0);
	glRotatef(5,1,0,0);
	drawA0087836Rectangle();
	glPopMatrix();
	glPushMatrix();
	glScalef(0.5,1.6,1);
	glTranslatef(0,1.2,0);
	glRotatef(5,1,0,0);
	drawA0087836Rectangle();
	glPopMatrix();
	glPushMatrix();
	glScalef(0.5,1.6,1);
	glTranslatef(0,-1.1,0);
	glRotatef(5,1,0,0);
	drawA0087836Rectangle();
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	//==DRAW HOUSEROOFFANS##==//

	//==##DRAW HOUSEROOFFANSHolder==//
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texSet[18]);
	glTranslatef(1,7,0);
	drawA0087836Sphere(0.4,0.4,0.4);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	//==DRAW HOUSEROOFFANSHolder##==//

	//==##DRAW HOUSEMainBody==//
	glPushMatrix();
	glRotatef(10,1,0,0);
	glPushMatrix();

	//DRAW HOUSEROOFBODY2
	glPushMatrix();
	glTranslatef(0,4.2,0);
	glScalef(0.5,0.8,0.5);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texSet[17]);
	drawA0087836Cube();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	//DRAW HOUSEROOFBODY2
	glPushMatrix();
	glTranslatef(-0.3,1,0);
	glScalef(1,2,3);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texSet[1]);
	drawA0087836Cube();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	//roof
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texSet[14]);
	glTranslatef(-0.3,0.7,0);
	glRotatef(90,0,1,0);
	glScalef(1,0.5,0.5);
	for(int i=0; i<2; i++){
		glBegin(GL_POLYGON); 
		glTexCoord2d(0,0);glVertex3f(-3, 8, 0);
		glTexCoord2d(1,0);glVertex3f(3, 8, 0);
		glTexCoord2d(1,1);glVertex3f(3, 3, 3);
		glTexCoord2d(0,1);glVertex3f(-3, 3, 3);
		glEnd();
		glRotatef(180,0,1,0);
	}
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	glPopMatrix();
	//==DRAW HOUSERMainBody##==//

	//==##DRAW HOUSERDoorway==//
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texSet[23]);
	glPushMatrix();
	glTranslatef(2,1,2.5);
	glRotatef(90,1,0,0);
	drawA0087836Cylinder(0.2,1);
	glPopMatrix();
	glPushMatrix();
	glTranslatef(2,1,1);
	glRotatef(90,1,0,0);
	drawA0087836Cylinder(0.2,1);
	glPopMatrix();
	glPushMatrix();
	glTranslatef(1.5,2,1.8);
	glScalef(1,0.1,1.2);
	drawA0087836Cube();
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glTranslatef(-0.29,1,1.9);
	glRotatef(90,0,1,0);
	glBindTexture(GL_TEXTURE_2D, texSet[5]);
	drawA0087836Rectangle();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();

	glEnable(GL_TEXTURE_2D);
	glTranslatef(-0.29,0.9,-1);
	glRotatef(90,0,0,1);
	glRotatef(90,1,0,0);
	glScalef(1,1,1);
	glBindTexture(GL_TEXTURE_2D, texSet[27]);
	drawA0087836Rectangle();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();

	glPopMatrix();
	//==DRAW HOUSERDoorway##==//
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glTranslatef(0,2.01,0);
	glRotatef(90,1,0,0);
	glScalef(3.9,3.9,2);
	glBindTexture(GL_TEXTURE_2D, texSet[10]);
	drawA0087836Rectangle();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	//==DRAW sinkhole##==//
	glPushMatrix();
	glColor3f(0,0,0);
	glTranslatef(0.5,-0.98,0);
	glRotatef(-90,1,0,0);
	drawA0087836Circle(3.2,3.9);
	glPopMatrix();
	//==DRAW sinkhole##==//

	//==DRAW HOUSE##==//
	///**
	//==##DRAW PORTAL SPACE==//
	glPushMatrix();
	//cornerstone
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture( GL_TEXTURE_2D, texSet[12] );
	glTranslatef(-2.7,0.5,0);
	drawA0087836Sphere(1.2,1.2,1.2);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	//gateway
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture( GL_TEXTURE_2D, texSet[20] );
	glTranslatef(-2.7,4.5,0);
	glRotatef(90,0,1,0);
	glScalef(3.5,6,2);
	glutSolidTorus(0.1, 1.0, 24, 24);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	glPopMatrix();
	//==DRAW PORTAL SPACE##==//
	//**/
	glPopMatrix();
}